# AWS Elastic Beanstalk Package - Dockerfile #

### What is this repository for? ###

Hold the Dockerfile for the aws-eb-package image.  This image expects a `package.sh` file in the current working directory
https://hub.docker.com/r/4mation/aws-eb-package/

### How do I get set up? ###

1. `docker build --tag 4mation/aws-eb-package:latest .`
1. `docker login`
1. `docker push 4mation/aws-eb-package` 

### how to use in Bitbucket Pipelines ###

```
# build and deploy kss applications to elastic beanstalk.
# Image used just contains the tools required, no packaging logic, that is all in package.sh
image: 4mation/aws-eb-package

pipelines:
  branches:
      master:
      - step:
          script:
            - bash package.sh
```